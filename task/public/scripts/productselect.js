// Script for passing selected products to php script, that will delete them

$(document).ready(function() {
    $( "#heading-button" ).click(function() {      
        var checkboxes = document.getElementsByClassName("selected");
        var skucodes = [];
        var items = [];
        var a = 0;
        for(var i = 0; i < checkboxes.length; i++){ // get all selected product sku's
            if(checkboxes[i].checked){
                skucodes[a] = checkboxes[i].value;
                items[a] = checkboxes[i].parentNode;
                a++;
            }
        }

        $.ajax({ // request to php script that will delete this items
            type: "POST",
            url: "../phpscripts/deleteproducts.php",
            data: {products: skucodes},
            success: function(result){
                if (items.length == 0) {
                    alert("Choose product to delete");
                } else { 
                    for(var i = 0; i < items.length; i++){
                        items[i].parentNode.removeChild(items[i]); 
                    }
                    alert("Products deleted");
                }     
            }
        });      
    });
});