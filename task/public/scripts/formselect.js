// script that sends request to php script to output required form. 
   
    $(document).ready(function() {
        $("#typeswitcher").change(function() {
        typename = $("#typeswitcher option:selected").val();
        $("#attribute-form").remove();
            $.ajax({
                type: "POST",
                url: '../phpscripts/newform.php',
                data: {product_type: typename},
                success: function(result){
                    $("#main").append(result);
                }
            });
        })
    });