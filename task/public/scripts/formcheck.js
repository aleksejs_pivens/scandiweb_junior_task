// input validation from client side
// Check if input is not blank and price with attributes are numeric and not null

$(document).ready(function() {
    $( "#heading-button" ).click(function() {
        skucode = $("#skucode").val();
        name = $("#name").val();
        price = $("#price").val(); 
        product_type = $("#typeswitcher option:selected").val();
        attribute_unit = $("#unit").text();
        attributes = new Object();
        valcheck = true; 

        // remove error messages
        $("#skucode-error").text("");
        $("#name-error").text("");
        $("#price-error").text(""); 
        
        i = 0;
        $("input[class='attribute']").each(function() {
            value = $(this).val();
            errornodes = document.getElementsByClassName("attribute-error");
        
            if (value == 0) {
                errornodes[i].textContent = "Attributes field is blank!";
                errornodes[i].style.color = "red";
                valcheck = false;   
            } else {
                attributes[$(this).attr('id')] = value;
                errornodes[i].textContent = "";
            }
            i++;
        });

        if (skucode == 0) {
            $("#skucode-error").text("SKU code is blank!");
            $("#skucode-error").css("color", "red");
            valcheck = false;
        } 

        if (name == 0) {
            $("#name-error").text("Product name is blank");
            $("#name-error").css("color", "red");
            valcheck = false;
        } 

        if (price == 0){
            $("#price-error").text("Product price is blank");
            $("#price-error").css("color", "red");
            valcheck = false;
        }

        if (valcheck == true) {
            productadd = productAdd(skucode, name, price, product_type, attributes, attribute_unit);
        }
    });
});