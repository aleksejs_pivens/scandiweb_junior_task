function productAdd(skucode, name, price, type, attributes, unit) {
    $.ajax({
        type: "POST",
        url: "../phpscripts/newproduct.php",
        data:{
        product_code: skucode, 
        product_name: name,
        product_price: price,
        product_type: type,
        attributes: attributes,
        attribute_unit: unit},
        dataType: "json",
        success: function(data){

            if(data.errors){ // check if we have some error messages
                alert("Product added");
                $('#main-form').trigger("reset");
                $('#attribute-form').trigger("reset"); 
            } else { // notify that product was added and clear forms
                    $("#skucode-error").text(data.skucode);
                    $("#skucode-error").css('color', 'red');      

                    $("#name-error").text(data.name);
                    $("#name-error").css('color', 'red');
    
                    $("#price-error").text(data.price);
                    $("#price-error").css('color', 'red');
    
                    attrmsg = data.attribute;
                    $(".attribute-error").each( function() {
                        $(this).text(attrmsg[$(this).attr('id')]);
                        $(this).css("color", "red");
                    });
            }
        }
    });
}