<!DOCTYPE html>
    <?php require __DIR__.'/../src/db/MySQLStorage.class.php'; // our storage class ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Store product list</title>

    <script src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>

    <script src="scripts/productselect.js"></script>
</head>
<body>
    <div id="header">
        <h1 id="header-heading">Product List</h1>
        <button id="heading-button">Delete Products</button>
    </div>
        <hr>
    <div class="grid-container">
   <?php // product list 

    $storage = new MySQLStorage(); 
    $products = $storage->getAllProducts(); // request to get products from db

    foreach ($products as $product) { // output products
        echo "<div class='grid-item'>
            <input type='checkbox' class='selected' value=". $product->getSKU() ."><br>
            <p>".$product->getSKU()."</p>
            <p>".$product->getName()."</p>
            <p>Price: ".$product->getPrice()." $</p>";      
            $values = json_decode($product->getAttribute(), true);
            foreach ($values as $x => $value) {
                echo $x . ": " . $value . " " . $product->getUnit();
            } 
        echo "</div>";
        }
    $storage->closeConnection(); ?>
    </div>
</body>
</html>