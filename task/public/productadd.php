<!DOCTYPE html>
    <?php  require __DIR__ .'/phpscripts/producttypes.php'; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css"> 
    <title>Store product add</title>

    <script src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>

    <script src="scripts/formselect.js"></script>
    <script src="scripts/submitform.function.js"></script>
    <script src="scripts/formcheck.js"></script>
</head>
<body>
    <div id="header">
        <h1 id="header-heading">Product add</h1>
        <button id="heading-button">Add product</button>
    </div>
        <hr>
    <div id="main">
        <form id="main-form", action="">
            <label for="skulabel">SKU</label> <br>
            <input type="text" id="skucode"> <br>
            <div id="skucode-error"></div> <br>

            <label for="namelabel">Name</label> <br>
            <input type="text" id="name"> <br>
            <div id="name-error"></div> <br>

            <label for="pricelabel">Price</label> <br>
            <input type="text" id="price"> <br>
            <div id="price-error"></div> <br>
        </form> 

        <label for="typeswitch">Type Switcher</label><br><br>
            <select id="typeswitcher">
                <option value="Disc">Disc</option>
                <option value="Book">Book</option>
                <option value="Furniture">Furniture</option>
            </select><br><br>

        <?php $types["Disc"]->getAttributeForm(); ?>
    </div>
</body>
</html>