<?php

require __DIR__ .'/../../src/db/MySQLStorage.class.php';

$storage = new MySQLStorage();
$products = $_POST['products'];

foreach ($products as $product) {
    $storage->deleteProduct($product);
}
$storage->closeConnection();