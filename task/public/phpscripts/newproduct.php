<?php

require __DIR__ . '/producttypes.php';
require_once __DIR__ . "/../../src/db/MySQLStorage.class.php";

$storage = new MySQLStorage();
$chk = true;
$skucode = $_POST['product_code'];
$name = $_POST['product_name'];
$type = $_POST['product_type'];
$pricepattern = "/^\d+(\.\d{2})?$/";
$price = $_POST['product_price'];
$attributes = $_POST['attributes'];
$unit = $_POST['attribute_unit'];
$error = [];

if ($storage->checkProductExist($skucode)) {
    $error['skucode'] = "The SKU code is already taken!";
    $chk = false;
}

if (!preg_match($pricepattern, $price)) {
    $error['price'] = "Price is invalid!";
    $chk = false;
}

$attrerror = [];
foreach ($attributes as $x => $val) {
    if (!is_numeric($val)){
        $attrerror[$x] = "Attribute is invalid!";
        $chk = false;
    }
}
$error['attribute'] = $attrerror;

if ($chk) {
    $types[$type]->fetchProduct($skucode, $name, $price, $type, $attributes, $unit);
    $storage->addProduct($types[$type]);
    $error["errors"] = true;
} else {
    $error["errors"] = false;
}

echo json_encode($error);
$storage->closeConnection();