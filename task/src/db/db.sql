CREATE DATABASE store;

use store;

CREATE TABLE product(
    SKU char(128) PRIMARY KEY,
    name char(128),
    type char(128),
    price float) engine=InnoDB;


CREATE TABLE attributes(
    product char(128) Primary key,
    attribute JSON,
    unit char(10),  
    FOREIGN KEY(product) REFERENCES product(SKU) ON DELETE CASCADE) engine=InnoDB;