<?php //Database connection class

class Connection
{
    protected $connection;
    private $host = 'localhost:3306';
    private $user = 'root';
    private $pass = '';
    private $db = 'store';

    public function __construct()
    {
        try {
            $this->connection = new mysqli($this->host, $this->user, $this->pass, null);
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->connection->exit("Error connecting to database");
        }

        if ($this->connection->select_db($this->db) === true) { // check if database exists
            error_log("Connected");
        } else {
            error_log("Can't Connect to database");
        }
    }

    public function closeConnection()
    {
        $this->connection->close();
    }

    public function getConnection()
    {
        return $this->connection;
    }
}