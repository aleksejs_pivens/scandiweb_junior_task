<?php

require 'Connection.class.php'; // connection to db
require 'model/Product.class.php'; // product model
require 'model/Furniture.class.php'; // furniture model
require 'model/Disc.class.php'; // disc model
require 'model/Book.class.php'; // book model
require 'interfaces/StorageInterface.php'; // storage interface

class MySQLStorage extends Connection implements StorageInterface
{
    public function getAllProducts() // get products for product list
    {
        $stmt = $this->connection->prepare("SELECT product.*, attributes.* FROM product INNER JOIN
        attributes ON product.SKU=attributes.product");

        $stmt->execute();
        $result = $stmt->get_result();
        $products = array();

        while ($row = $result->fetch_assoc()) {  
            $product = new Product();
            $product->setProduct($row["SKU"], $row["name"], $row["price"], $row["attribute"], $row["unit"]);
            $products[] = $product;
        }
        return $products;
    }

    public function getAllTypes() // get all product types for type switcher
    {
        $types = array();

        $disc = new Disc();
        $types["Disc"] = $disc;
        
        $book = new Book();
        $types["Book"] = $book;

        $furniture = new Furniture();
        $types["Furniture"] = $furniture;

        return $types;
    }

    public function checkProductExist($sku) // check if product with sku exists
    {
        $check = false;
        $stmt = $this->connection->prepare("SELECT * FROM product WHERE SKU=?");
        $stmt->bind_param("s", $sku);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->fetch_assoc() > 0) {
            $check = true;
        }

        $stmt->close();
        return $check;
    }

    public function addProduct($product) // adding product
    {
        $sku = $product->getSKU();
        $name = $product->getName();
        $type = $product->getType();
        $price = $product->getPrice();
        $attributes = json_encode($product->getAttribute());
        $unit = $product->getUnit();
        $stmt = $this->connection->prepare("INSERT INTO product(SKU, name, type, price)
        VALUES(?, ?, ?, ?);");
        $stmt->bind_param("sssd", $sku, $name, $type, $price);

        if ($stmt->execute() === true) {
            error_log("added product");
        } else {
            error_log($stmt->error);
        }

        $stmt = $this->connection->prepare("INSERT INTO attributes(product, attribute, unit)
        VALUES(?, ?, ?);");
        $stmt->bind_param("sss", $sku, $attributes, $unit);

        if ($stmt->execute() === true) {
            error_log("added attributes");
        } else {
            error_log($stmt->error);
        }
    }

    public function deleteProduct($skucode) // delete product 
    {
        $stmt = $this->connection->prepare("DELETE FROM product WHERE SKU=? ;");
        $stmt->bind_param("s", $skucode);

        if ($stmt->execute() === true) {
            error_log("products deleted");
        } else {
            error_log($stmt->error);
        }
    }
}