<?php // interface for specific product types
 
Interface ProductTypeInterface
{
    public function fetchProduct($sku, $name, $price, $type, $attribute, $unit);

    public function getAttributeForm();
}