<?php

Interface StorageInterface
{ 
    public function getAllProducts();

    public function getAllTypes();

    public function checkProductExist($sku);

    public function addProduct($product);

    public function deleteProduct($SKUcode);
}