<?php 

require_once "Product.class.php";
require_once __DIR__ . "/../interfaces/ProductTypeInterface.php";

// Product type Book
class Book extends Product implements ProductTypeInterface
{
    // Method for adding product with type Book
    public function fetchProduct($sku, $name, $price, $type, $attribute, $unit)
    {
        $this->setSKU($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setType($type);
        $this->setAttribute($attribute);
        $this->setUnit($unit);
    }

    public function getAttributeForm()
    {
        echo '<form id="attribute-form">   
            <label for="Weight">Weight</label>
            <input type="text" class="attribute" id="Weight" value="">
            <label for="Kg" id="unit">Kg</label><br>
            <div class="attribute-error" id="Weight"></div> <br>
            <div>Please, provide weight in Kg</div>
            </form>';
    }
}