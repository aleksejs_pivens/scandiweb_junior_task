<?php 

require_once "Product.class.php";
require_once __DIR__ . "/../interfaces/ProductTypeInterface.php";

// Product type Disc
class Disc extends Product implements ProductTypeInterface
{
    // method for adding product with type Disc
    public function fetchProduct($sku, $name, $price, $type, $attribute, $unit)
    {
        $this->setSKU($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setType($type);
        $this->setAttribute($attribute);
        $this->setUnit($unit);
    }

    public function getAttributeForm()
    {
        echo '<form id="attribute-form">   
            <label for="Size">Size</label>
            <input type="text" class="attribute" id="Size" value="">
            <label for="Mb" id="unit">Mb</label><br>
            <div class="attribute-error" id="Size"></div> <br>
            <div>Please, provide Size in Mb</div>
            </form>';
    }
}