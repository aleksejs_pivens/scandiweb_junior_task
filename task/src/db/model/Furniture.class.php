<?php 

require_once "Product.class.php";
require_once __DIR__ . "/../interfaces/ProductTypeInterface.php";     

// Product type Furniture
class Furniture extends Product implements ProductTypeInterface
{
    // method for adding product with type Furniture and converting it's attributes into 1 attribute called Dimensions
    public function fetchProduct($sku, $name, $price, $type, $attribute, $unit)  
    {
        $this->setSKU($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setType($type);
        $value = $attribute["Height"] . "x" . $attribute["Width"] . "x" . $attribute["Length"];
        $this->setAttribute(array("Dimensions"=>$value));
        $this->setUnit(null);
    }

    // method for outputing form for Furniture attribute
    public function getAttributeForm()
    {
        echo '<form id="attribute-form">   
            <label for="Height">Height</label>
            <input type="text" class="attribute" id="Height" value="">
            <label for="m" id="unit">m</label><br>
            <div class="attribute-error" id="Height"></div> <br>

            <label for="Width">Width</label>
            <input type="text" class="attribute" id="Width" value="">
            <label for="m" id="unit">m</label><br>
            <div class="attribute-error" id="Width"></div> <br>

            <label for="Length">Length</label>
            <input type="text" class="attribute" id="Length" value="">
            <label for="m" id="unit">m</label><br>
            <div class="attribute-error" id="Length"></div> <br>
            <div>Please, provide Dimensions in HxWxL</div>
            </form>';
    }
}