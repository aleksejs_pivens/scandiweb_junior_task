<?php

class Product
{
    private $sku;
    private $name;
    private $price;
    private $type;
    private $attribute;
    private $unit;

    public function setProduct($sku, $name, $price, $attribute, $unit)
    {
        $this->setSKU($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setAttribute($attribute);
        $this->setUnit($unit);
    }

    public function setSKU($sku)
    {
        $this->sku = $sku;
    } 

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    public function getSKU()
    {
        return $this->sku;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getAttribute()
    {
        return $this->attribute;
    }

    public function getUnit()
    {
        return $this->unit;
    }
}